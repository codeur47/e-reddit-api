package com.yorosoft.eredditapi.exception;

public class SubredditNotFoundException extends  RuntimeException{
    public SubredditNotFoundException(String message) {
        super(message);
    }
}
