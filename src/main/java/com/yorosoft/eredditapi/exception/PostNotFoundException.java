package com.yorosoft.eredditapi.exception;

public class PostNotFoundException extends RuntimeException {
    public PostNotFoundException(String message) { super(message); }
}
