package com.yorosoft.eredditapi.exception;

public class SpringRedditException extends RuntimeException{
    public SpringRedditException(String message) { super(message); }
}
