package com.yorosoft.eredditapi.repository;

import com.yorosoft.eredditapi.model.Comment;
import com.yorosoft.eredditapi.model.Post;
import com.yorosoft.eredditapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);

}
