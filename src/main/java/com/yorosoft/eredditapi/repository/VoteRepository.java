package com.yorosoft.eredditapi.repository;

import com.yorosoft.eredditapi.model.Post;
import com.yorosoft.eredditapi.model.User;
import com.yorosoft.eredditapi.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
