package com.yorosoft.eredditapi.repository;

import com.yorosoft.eredditapi.model.Post;
import com.yorosoft.eredditapi.model.Subreddit;
import com.yorosoft.eredditapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);

}
