package com.yorosoft.eredditapi;

import com.yorosoft.eredditapi.config.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@Import(SwaggerConfiguration.class)
public class ERedditApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(ERedditApiApplication.class, args);
	}
}
